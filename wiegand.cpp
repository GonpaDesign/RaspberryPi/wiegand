#include "wiegand.h"			/* Headers */
#include "wiegandStr.h"			/* Structs */
#include <wiringPi.h>			/* */
#include <stdio.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <jsoncpp/json/json.h> 	/* Json parser*/ // or jsoncpp/json.h , or json/json.h etc.
#include <sys/socket.h> 		/* socket, connect */
#include <netinet/in.h> 		/* struct sockaddr_in, struct sockaddr */
#include <netdb.h> 				/* struct hostent, gethostbyname */
#include <string.h>				/* memcpy, memset */
#include <string>
#include <thread>

using namespace std;

volatile long long _receivedCard = 0;
volatile long long _code = 0;
volatile long long _CSN = 0;
volatile int _bitCount=0;	
volatile unsigned long _lastWiegand=0;
volatile unsigned long _Mask=0;

const strWiegandParity defaultParity;
const strWiegandFacilityCode defaultFacilityCode;
const strWiegandCustomData defaultCustomData;
const strWiegandConfig defaultWiegandFormat;

#define LOG_FILE "/var/log/wiegand/event.log"
#define CARD_FILE "/var/log/wiegand/card"

#define SYSTEM_CONFIG_FILE "/usr/programs/wiegand/system.json"
#define WIEGAND_CONFIG_FILE "/usr/programs/wiegand/wiegand.json"
#define WEB_CONFIG_FILE "/usr/programs/wiegand/webClient.json"

bool defaultFormat = false;
int _waitTime = 0;
long lastCheck = 0;
long conf = 5;
time_t lastMod;

void log(int type, char* description)
{
	time_t rawTime;
	struct tm * timeinfo;
	
	time(&rawTime);
	timeinfo = localtime (&rawTime);
	
	FILE *f = fopen(LOG_FILE, "a");
	
	if(f == NULL)
	{
		printf("Error opening file");
	}
	
	char Date[20];
	sprintf(Date, "%02d/%02d/%d  %02d:%02d:%02d", timeinfo->tm_mday, timeinfo->tm_mon + 1,timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
	
	switch(type)
	{
		case 0:
			fprintf(f, "%s	%s	%s\n", Date, "DEBUG", description);
		break;
		case 1:
			fprintf(f, "%s	%s	%s\n", Date, "INFO", description);
		break;
		case 2:
			fprintf(f, "%s	%s	%s\n", Date, "ERROR", description);
		break;
	}
	
	fclose(f);
}

void printLongBinary(unsigned long number)
{
	unsigned long tempNumber;
	for(int LSBbit = 0; LSBbit < 32; LSBbit++)
	{
		tempNumber = number;
		int MSBbit = 31 - LSBbit;
		tempNumber >>= MSBbit;
		int m = (MSBbit + 1) % 4;
		if((m == 0) && (MSBbit != 31)){printf(" ");}
		printf("%c", tempNumber & 0x01 ? '1' : '0');
	}
	printf("\n"); 
	fflush(stdout);
}

std::string getBinary(unsigned long number)
{
	std::string binaryNumber;
	unsigned long tempNumber;
	for(int LSBbit = 0; LSBbit < 32; LSBbit++)
	{
		tempNumber = number;
		int MSBbit = 31 - LSBbit;
		tempNumber >>= MSBbit;
		int m = (MSBbit + 1) % 4;
		if((m == 0) && (MSBbit != 31)){binaryNumber += " ";}
		if(tempNumber & 0x01)
		{
			binaryNumber += "1";
		}
		else
		{
			binaryNumber += "0";
		}
	}
	return binaryNumber;
}

void printLongLongBinary(long long number)
{
	for(int LSBbit = 0; LSBbit < Format.Size; LSBbit++)
	{
		long long tempNumber = number;
		int MSBbit = (Format.Size - 1) - LSBbit;
		tempNumber >>= MSBbit;
		int m = (MSBbit + 1) % 4;
		if((m == 0) && (MSBbit != (Format.Size - 1))){printf(" ");}
		printf("%c", tempNumber & 0x000000000000001 ? '1' : '0');
	}
	printf("\n"); 
	fflush(stdout);
}

std::string getLongLongBinary(long long number)
{
	std::string binaryNumber;
	for(int LSBbit = 0; LSBbit < Format.Size; LSBbit++)
	{
		long long tempNumber = number;
		int MSBbit = (Format.Size - 1) - LSBbit;
		tempNumber >>= MSBbit;
		int m = (MSBbit + 1) % 4;
		if((m == 0) && (MSBbit != (Format.Size - 1))){binaryNumber += " ";}
		if(tempNumber & 0x000000000000001)
		{
			binaryNumber += "1";
		}
		else
		{
			binaryNumber += "0";
		}	
	}
	return binaryNumber;
}

void ReadD0 ()
{
	_bitCount++;				// Incrementa el contador de bits
	_receivedCard <<=1;
	_lastWiegand = millis();	// Actualiza el registro de la ultima interrupcion
}

void ReadD1()
{
	_bitCount++;				// Incrementa el contador de bits
	_receivedCard <<=1;
	_receivedCard |= 1;
	
	_lastWiegand = millis();	// Actualiza el registro de la ultima interrupcion
}

void getFacilityCode()
{
	if(defaultFormat)
	{
		long long _tempMask = 0;
		//int j;
		for(int i = 0; i < defaultWiegandFormat.Size; i++)
		{
			//j = defaultWiegandFormat.Size -i;
			if((i >= defaultFacilityCode.bin) && (i <= defaultFacilityCode.ben))
			{
				_tempMask <<= 1;
				_tempMask |= 1;		
			}
			else
			{
				_tempMask <<= 1;
			}
		}
		long long _tempData = _receivedCard;
		_tempData &= _tempMask;
		_tempData >>=  defaultWiegandFormat.Size - defaultFacilityCode.ben;
		received.facilityCode = _tempData;
	}
	else
	{
		if(FacilityCode.enabled == 1)
		{
			long long _tempMask = 0;
			int j;
			for(int i = 0; i < Format.Size; i++)
			{
				j = Format.Size -i;
				if((j >= FacilityCode.bin) && (j <= FacilityCode.ben))
				{
					_tempMask |= 1;
					_tempMask <<= 1;
				}
				else
				{
					_tempMask <<= 1;
				}
			}
			
			long long _tempData = _receivedCard;
			_tempData &= _tempMask;
			_tempData >>= ((Format.Size - FacilityCode.ben) - 1);
			received.facilityCode = _tempData;
		}
	}
}

void getCustomData()
{
	if(defaultFormat == true){	
		long long _tempMask = 0;
		
		for(int i = 0; i < defaultWiegandFormat.Size; i++)
		{
			int j = defaultWiegandFormat.Size -i;
			if((j >= defaultCustomData.bin[0]) && (j <= defaultCustomData.ben[0]))
			{
				_tempMask |= 1;
				_tempMask <<= 1;
			}
		}
	
		long long _tempData = _receivedCard;
		_tempData &= _tempMask;
		_tempData >>= ((defaultWiegandFormat.Size - defaultCustomData.ben[0]) - 1);
		received.code[0] = _tempData;
	}
	else
	{
		for(int cdIndex = 0; cdIndex < 8; cdIndex++)
		{
			if(CustomData.enabled[cdIndex] != 0)
			{		
				long long _tempMask = 0;
				
				for(int i = 0; i < Format.Size; i++)
				{
					int j = Format.Size -i;
					if((j >= CustomData.bin[cdIndex]) && (j <= CustomData.ben[cdIndex]))
					{
						_tempMask |= 1;
						_tempMask <<= 1;
					}
				}
			
				long long _tempData = _receivedCard;
				_tempData &= _tempMask;
				_tempData >>= ((Format.Size - CustomData.ben[cdIndex]) - 1);
				received.code[cdIndex] = _tempData;
			}
			else
			{
				received.code[cdIndex] = 0;
			}
		}
	}
}
	
int checkParity()
{
	for(int pIndex = 0; pIndex < 8; pIndex++)
	{
		if(Parity.enabled[pIndex] != 0)
		{
			long long p = 0;
			p = Parity.maskHigh[pIndex];
			p <<= 32;
			p |= Parity.maskLow[pIndex];
			if(systemConfiguration.debug){ printf("parity mask:	"); printLongLongBinary(p);}
			p &= _receivedCard;
			if(systemConfiguration.debug){ printf("parity data:	"); printLongLongBinary(p);}
			
			int sum = 0, b = 1;
			
			for(int i = 0; i < 64; i++)
			{
				if(p & (b << i)){sum++;}
			}
			
			sum += Parity.type[pIndex];
			int type = sum % 2;
						
			long long pr = _receivedCard;
			long long f = 1;
			f <<= Parity.position[pIndex];
			f &= pr;
			f >>= Parity.position[pIndex];
			
			if(systemConfiguration.debug){printf("%s %lld\n", "Parity received: ", f);}
			
			if(type != f)
			{
				if(systemConfiguration.debug){printf("%s %d %s %d\n", "Error - Parity sum: ", type, ", Parity type: ", Parity.type[pIndex]);}
				return 0;
			}
		}
	}
	return 1;
}

void getCSN()
{
	_CSN = _receivedCard;
	_CSN &= 33554430;
	_CSN >>= 1;
}

int generateCardID()
{
	if(!defaultFormat)
	{
		if(checkParity() != 1)
		{
			char msg[256];
			sprintf(msg, "%s %lld", "Communication error, check parity failed. received data: ", _receivedCard);
			log(2, msg);
			return 0;
		}
	}else{
		getCSN();
	}
	getCustomData();
	getFacilityCode();
	return 1;
}

bool DoWiegandConversion ()
{
	//unsigned long sysTick = millis();
	
	if ((millis() - _lastWiegand) >= Format.Interval)						// Si no se detectan pulsos por mas de 25ms
	{
		if (_bitCount == Format.Size || _bitCount == 26)
		{
			if(_bitCount == 26){
				defaultFormat = true;
			}
			if(generateCardID() != 0)
			{
				cout << getBinary(_receivedCard) << endl;
				_code = _receivedCard;
				_bitCount = 0;
				_receivedCard = 0;
				return true;	
			}
			else
			{
				_bitCount = 0;
				_receivedCard = 0;
				return false;
			}
		}
		else
		{																// Si falla la decodificacion. Se elimina la informacion guardada.															
			_lastWiegand = millis();
			_bitCount = 0;			
			_receivedCard = 0;
			return false;
		}	
	}
	else
	{
		return false;
	}
}

int file_is_modified(const char *path, time_t oldMTime) {
    struct stat file_stat;
    int err = stat(path, &file_stat);
    if (err != 0) {
        perror(" [file_is_modified] stat");
    }
    return file_stat.st_mtime > oldMTime;
}

int readWiegandConfiguration()
{
	try
	{
		ifstream systemConfig(WIEGAND_CONFIG_FILE);
		Json::Reader reader;
		Json::Value jsonObj;
		reader.parse(systemConfig, jsonObj); 
		
		// Carga de configuracion de Formato
		const Json::Value& format = jsonObj["format"]; // array of characters
		Format.Size = format["size"].asInt();
		Format.Pulse = format["pulseTime"].asInt();
		Format.Interval = format["intervalTime"].asInt();
		Format.LSB = format["LSB"].asInt();
		Format.error = format["errorCode"].asInt();
		
		// Carga de configuracion de Facility Code
		const Json::Value& facilityCode = jsonObj["facilityCode"]; // array of characters
		FacilityCode.enabled = facilityCode["enabled"].asInt();
		FacilityCode.bin = facilityCode["initialBit"].asInt();
		FacilityCode.ben = facilityCode["endBit"].asInt();
		FacilityCode.value = facilityCode["value"].asInt();

		// Carga de configuracion de Custom Data
		for(int index = 0; index < 8; index++)
		{		
			const Json::Value& data = jsonObj["customData"]; // array of characters
			CustomData.enabled[index] = data["enabled"][index].asInt();
			CustomData.bin[index] = data["initialBit"][index].asInt();
			CustomData.ben[index] = data["endBit"][index].asInt();
			CustomData.value[index] = data["value"][index].asInt();
		}
		
		// Carga de configuracion de paridad
		for(int index = 0; index < 8; index++)
		{		
			const Json::Value& par = jsonObj["parity"]; // array of characters
			Parity.enabled[index] = par["enabled"][index].asInt();
			Parity.type[index] = par["parity"][index].asInt();
			Parity.position[index] = par["bit"][index].asInt();
			Parity.maskLow[index] = par["lowMask"][index].asUInt();
			Parity.maskHigh[index] = par["highMask"][index].asUInt();
		}
		
		if(systemConfiguration.debug == 1)
		{
			cout << "--- Wiegand configuration ---" << endl;
			cout << "--- Format ---" << endl;
			cout << "Format Size: " << Format.Size << endl;
			cout << "Format Pulse time: " << Format.Pulse << endl;
			cout << "Format Interval time: " << Format.Interval << endl;
			cout << "Format LSB: " << Format.LSB << endl;
			cout << "Format Error code: " << Format.error << endl;
			cout << endl;
			cout << "--- Facility Code ---" << endl;
			cout << "Facility Code enabled: " << (FacilityCode.enabled == 1 ? "True" : "False") << endl;
			if(FacilityCode.enabled)
			{
				cout << "Facility Code initial bit: " << FacilityCode.bin << endl;
				cout << "Facility Code end bit: " << FacilityCode.ben << endl; 
				cout << "Facility Code value: " << FacilityCode.value << endl;
			}
			cout << endl;
			cout << "--- Custom Data ---" << endl;
			for(int index=0; index < 8; index++){
				if(CustomData.enabled[index]){
					cout << " --- Custom Data index: " << index << endl;
					cout << "Custom Data enabled: " << (CustomData.enabled[index] == 1 ? "True" : "False") << endl;
					cout << "Custom Data initial bit: " << CustomData.bin[index] << endl;
					cout << "Custom Data end bit: " << CustomData.ben[index] << endl;
					cout << "Custom Data default value: " << CustomData.value[index] << endl;
					cout << endl;
				}
			}
			cout << "--- Parity ---" << endl;
			for(int index=0; index < 8; index++){
				if(Parity.enabled[index])
				{
					cout << " --- Parity index: " << index << endl;
					cout << "Parity index: " << index << endl;
					cout << "Parity position: " << Parity.position[index] << endl;
					cout << "Parity type: " << (Parity.type[index] == 1 ? "Odd" : "Even") << endl;
					cout << "Low  mask: " << getBinary(Parity.maskLow[index]) << endl;
					cout << "High mask: " << getBinary(Parity.maskHigh[index]) << endl;
					cout << endl;
				}
			}
		}
		return 1;
	}catch(exception& e){
		return 0;
	}
}

int readSystemConfiguration()
{
	try
	{
		ifstream systemConfig(SYSTEM_CONFIG_FILE);
		Json::Reader reader;
		Json::Value jsonObj;
		reader.parse(systemConfig, jsonObj); 
		const Json::Value& characters = jsonObj["system"]; // array of characters
		systemConfiguration.WD0 = characters["WD0pin"].asInt();
		systemConfiguration.WD1 = characters["WD1pin"].asInt();
		systemConfiguration.debug = characters["debug"].asInt();
		
		if(systemConfiguration.debug){
			cout << endl;
			cout << "--- System configuration ---\n" << endl;
			cout << "Wiegand Data 0 pin: " << systemConfiguration.WD0 << endl;
			cout << "Wiegand Data 1 pin: " << systemConfiguration.WD1 << endl;
			cout << "debug level: " << systemConfiguration.debug << endl;
			cout << endl;
		}
		
		return 1;
	}catch(exception& e){
		return 0;
	}
}

int readWebClient()
{
	try
	{
		ifstream systemConfig(WEB_CONFIG_FILE);
		Json::Reader reader;
		Json::Value jsonObj;
		reader.parse(systemConfig, jsonObj); 
		const Json::Value& characters = jsonObj["client"]; // array of characters
		webClient.method = characters["method"].asString();
		webClient.IP = characters["IP"].asString();
		webClient.port = characters["port"].asInt();
		webClient.id = characters["id"].asInt();
		webClient.URL = characters["URL"].asString();
		webClient.enable = characters["enable"].asInt();

		if(systemConfiguration.debug){
			cout << endl;
			cout << "--- System configuration ---\n" << endl;
			cout << "device ID: " << webClient.id << endl;
			cout << "IP: " << webClient.IP << endl;
			cout << "port: " << webClient.port << endl;
			cout << "Method: " << webClient.method << endl;
			cout << "URL: " << webClient.URL << endl;
			cout << "Enable: " << (webClient.enable ? "on" : "off") << endl;
			cout << endl;
		}
		
		return 1;
	}catch(exception& e){
		return 0;
	}
}

int requestHTTP(unsigned long dni)
{
	struct hostent *server;
	struct sockaddr_in serv_addr;
	int sockfd, bytes, sent, received, total;
	char message[512];
	char response[4096];
	sprintf(message, "%s %s?documento=%lu HTTP/1.1\r\nUser-Agent: Mozilla/5.0\r\nHost: %s:%d\r\nContent-Type: application/json\r\nConnection: close\r\n\r\n", webClient.method.c_str(), webClient.URL.c_str(), dni, webClient.IP.c_str(), webClient.port);
	
	// What are we going to send? 
	if(systemConfiguration.debug)
	{
		printf("Request:\n%s\n",message);
	}
	
	// create the socket
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) 
	{
		char msg[] = "Unable to open socket";
		log(2, msg);
		return 0;
	}
	
	// lookup the ip address
	server = gethostbyname(webClient.IP.c_str());
	if (server == NULL) 
	{
		char msg[] = "Host not found";
		log(2, msg);
		return 0;
	}	
	
	// fill in the structure
	memset(&serv_addr,0,sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(webClient.port);
	memcpy(&serv_addr.sin_addr.s_addr,server->h_addr,server->h_length);

	// connect the socket
	if (connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0)
	{
		char msg[] = "Unable to connect";
		log(2, msg);
        return 0;
	}

	// send the request
	total = strlen(message);
	sent = 0;
	do {
		bytes = write(sockfd,message+sent,total-sent);
		if (bytes < 0)
			cout << "ERROR writing message to socket";
		if (bytes == 0)
			break;
		sent+=bytes;
	} while (sent < total);

	// receive the response
	memset(response,0,sizeof(response));
	total = sizeof(response)-1;
	received = 0;
	do {
		bytes = read(sockfd,response+received,total-received);
		if (bytes < 0)
			cout << "ERROR reading response from socket";
		if (bytes == 0)
			break;
		received+=bytes;
	} while (received < total);

	if (received == total)
	{
		cout << "ERROR storing complete response from socket";
        return 0;
	}

	if (systemConfiguration.debug) 
	{
		printf(response);
		//char *pend = strrchr(response, '{');
		//int pendLen = strlen(pend);
		//pend[pendLen - 6] = 0;
		//log(3,pend);
	}
	
	// close the socket 
	close(sockfd);
	
    return 1;
}

void inform(int code)
{
	if(!requestHTTP(code))
	{
		cout << "Unable to request HTTP" << endl;
	}
}

int begin()
{
	_lastWiegand = millis();
	_code = 0;
	_bitCount = 0;
	_receivedCard = 0;
	pinMode(systemConfiguration.WD0, INPUT);												// Establece el pin D0 como entrada
	pinMode(systemConfiguration.WD1, INPUT);												// Establece el pin D1 como entrada
	if(wiringPiISR(systemConfiguration.WD0, INT_EDGE_FALLING, ReadD0) < 0){return 0;}		// Establece una interrupcion en el pin 0, de pulso alto a bajo
	if(wiringPiISR(systemConfiguration.WD1, INT_EDGE_FALLING, ReadD1) < 0){return 0;}		// Establece una interrupcion en el pin 1, de pulso alto a bajo
	return 1;
}

std::string Date(){
	time_t rawTime;
	struct tm * timeinfo;
	
	time(&rawTime);
	timeinfo = localtime (&rawTime);
	
	
	char Date[20];
	sprintf(Date, "%02d/%02d/%d  %02d:%02d:%02d", timeinfo->tm_mday, timeinfo->tm_mon + 1,timeinfo->tm_year + 1900, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
	return Date;
}

int eventLog(){
	FILE *f = fopen(LOG_FILE, "a");
	if(f == NULL)
	{
		printf("Error opening log file");
		return(1);
	}
	if(defaultFormat)
	{
		fprintf(f, "%s	%s	%s%i\n", Date().c_str(), "INFO", "Wiegand Type: ", defaultWiegandFormat.Size);
		fprintf(f, "%s	%s	%s%lu\n", Date().c_str(), "TRACE", "Facility Code: ",received.facilityCode);
		fprintf(f, "%s	%s	%s%lu\n", Date().c_str(), "TRACE", "Card Number: ",received.code[0]);
		fprintf(f, "%s	%s	%s%lld\n", Date().c_str(), "TRACE", "CSN: ",_CSN);
	}
	else
	{
									   fprintf(f, "%s	%s	%s%i\n", Date().c_str(), "INFO", "Wiegand Type: ", Format.Size);
		if(FacilityCode.enabled == 1){ fprintf(f, "%s	%s	%s%lu\n", Date().c_str(), "TRACE", "Facility Code: ",received.facilityCode);}
		for(int index = 0; index < 8; index++)
		{
			if(CustomData.enabled[index] == 1)
			{
				if(index == 0)
				{
					fprintf(f, "%s	%s	%s%lu\n", Date().c_str(), "TRACE", "Card Number: ",received.code[index]);
				}
				else
				{
					fprintf(f, "%s	%s	%s%d%s%lu\n", Date().c_str(), "TRACE", "Custom Data ", index, ": ",received.code[1]);
				}			
			}
			else
			{
				break;
			}
		}
	}
	fclose(f);
	return 1;
}

int wiegandLog(){
	Json::Value value_obj;
	value_obj["ID"] = std::to_string(received.code[0]);
	value_obj["Date"] = Date();
	
	std::ofstream jsonFile;
	jsonFile.open("event/card.json");
	Json::StyledWriter styledWriter;
	jsonFile << styledWriter.write(value_obj);
	jsonFile.close();
			
	return 1;
}

int main()
{
	cout << "Wiegand reader init" << endl;
	if(wiringPiSetup() < 0)
	{
		char msg[] = "Unable to setup wiringPi";
		log(2, msg);
		cout << "Unable to setup wiringPi, closing program..." << endl;
		return 0;
	}

	/*
	if(piHiPri(99) < 0)
	{
		cout << "Unable to set high priority process" << endl;
		char msg[] = "Unable to setup priority";
		log(2, msg);
	} 
	*/
	
	if(!readSystemConfiguration())
	{
		cout << "Unable to read system configuration" << endl;
		return 0;
	}
	
	if(!readWiegandConfiguration())
	{
		cout << "Unable to read Wiegand configuration" << endl;
		return 0;
	}
	
	if(!readWebClient()){
		cout << "Unable to read web configuration" << endl;
		return 0;
	}

	if(!begin()){return 0;}
	
	for(;;)
	{
		if(DoWiegandConversion() == true)
		{	
			
			if(!eventLog())
			{
				cout << "Unable to log event" << endl;
				//return 0;
			}
			
			if(!wiegandLog())
			{
				cout << "Unable to log code" << endl;
				//return 0;
			}
			
			if(webClient.enable)
			{
				if(defaultFormat)
				{
					thread t1 (inform, _CSN);
					t1.detach();
				}
				else
				{
					thread t1 (inform, received.code[0]);
					t1.detach();
				}
			}

			defaultFormat = false;
		}
		else
		{
			delayMicroseconds(Format.Interval * 2);
		}	
		/*
		// Reviso archivo de configuracion. Si fue modificado se lee nuevamente.
		if(time(NULL) >= lastCheck + conf){
			if(file_is_modified("wiegand.json", lastMod))
			{
				readWiegandConfiguration();
			}
			lastCheck = time(NULL);
		}	
		* */
	}
	return 0;
}
