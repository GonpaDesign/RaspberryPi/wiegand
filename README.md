WIEGAND DECODER
=======

## Descripción

Programa para leer serie de pulsos wiegand en 2 pines configurables de la Raspberry Pi 3 Model B+. 

El sistema necesita ser ejecutado con prioridad RT (realTime), para poder mejorar la precision de lectura de pulsos.

El funcionamiento del sistema en lineas generales, acumula los pulsos dentro de una (o mas variables). Cuando no se detectan pulsos por un tiempo mayor al Intervalo, se trata de analizar los pulsos acumulados (Se verifica paridades previamente). El procesamiento será de acuerdo a la cantidad de pulso que lleguen. Se puede utilizar un formato 26 Standard, en paralelo a otro formato definido en los archivos de configuración. Obtenido el valor del ID, se envia por un thread nuevo a la API configurada.

## Programación, librerias y compilación

El programa se encuentra escrito en C++, y compilado desde el Geany de la Raspberry Pi.

Se requiere tener instalada las librerias:

1. wiringPi (Viene preinstalada en el Raspbian)
2. jsoncpp

Para poder compilarlo hay que configurar el builder del geany y que el BUILD figure de la siguietne forma:

g++ -Wall -o "%e" "%f" -lwiringPi -ljsoncpp -lpthread

En cuanto a los pines utilizados, normalmente utilizamos la configuracion WD0 -> GPIO_GEN4 (GPIO23) y WD1 -> GPIO_GEN5 (GPIO24). La numeración a utilizar corresponde a la nomenclatura según wiringPi.

## Instalación

Para instalar el software debemos tener instalado los paquetes:

* libjsoncpp1
* libjsoncpp-dev

La ruta de instalacion (Se debe crear si no existe), es:

/usr/programs/wiegand/

Los logs, se guardarna en la siguiente ubicacion (Se debe crear si no existe):

/var/log/wiegand/event.log

## Configuración

Se cuenta con diversos archivos de configuracion. A continuacion se los enumera y detalla su uso.

*system.json

    Aqui se configuran los pines a utilizar para la lectura de los WD0 y WD1, ademas que se puede activar o desactivar el modo 
    DEBUG para obtener como salida todos los procesos en ejecucion durante el uso del codigo

*wiegand.json

    Aqui se configuran las paridades, tipos de datos, facility code, etc. 
    
*webClient.json

    Aqui configuramos el reporte del codigo a alguna API para su posterior procesamiento, puede ser a un HOST local como no.
    
## Ejecución

Se provee un Script para ejecutar el programa que automaticamente luego de iniciarlo lo pasa a prioridad RT.