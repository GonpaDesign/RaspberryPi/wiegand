#include <string>

struct strWiegandConfig{
	int Size = 26;
	unsigned int Pulse = 25;
	unsigned int Interval = 200;
	int LSB = 1;
	int error = 0;
} Format;

struct strWiegandParity{
	int enabled[8] = {1,1,0,0,0,0,0,0};
	int type[8] = {1,0,0,0,0,0,0,0};
	int position[8] = {0,25,0,0,0,0,0,0};
	unsigned long maskLow[8] = {33546240,8190,0,0,0,0,0,0};
	unsigned long maskHigh[8] = {0,0,0,0,0,0,0,0};
} Parity;

struct strWiegandFacilityCode{
	int enabled = 1;	
	int bin = 1;
	int ben = 9;
	unsigned long value = 0;
} FacilityCode;

struct strWiegandCustomData{
	int enabled[8] = {1,0,0,0,0,0,0,0};
	int bin[8] = {9,0,0,0,0,0,0,0};
	int ben[8] = {24,0,0,0,0,0,0,0};
	unsigned long value[8] = {0,0,0,0,0,0,0,0};
} CustomData;

struct strWiegandReceived{
	unsigned long code[8] = {0,0,0,0,0,0,0,0};
	unsigned long facilityCode = 0;
} received;

struct strSystem{
	int WD0 = 0;
	int WD1 = 2;
	int debug = 0; 
} systemConfiguration;

struct strWebClient{
	std::string method = "GET";
	int port = 80;
	std::string IP = "0.0.0.0";
	std::string URL = "";
	int id = 0;
	int enable = 0;
} webClient;
